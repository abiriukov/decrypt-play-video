#!/bin/sh
# Decode a file and play it with VLC

# Output file name
OUTPUT_FILE="temp"

# Decrypt the file
echo "Decrypting " $1 " into " $OUTPUT_FILE 
gpg --batch -d --passphrase-file ~/.passphrase $1 > $OUTPUT_FILE

# Open the output file with VLC
echo "Opening " $OUTPUT_FILE
vlc $OUTPUT_FILE

# Delete the output file
echo "Deleting " $OUTPUT_FILE
rm $OUTPUT_FILE
